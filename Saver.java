import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Saver
	{
		
	/**
	 * Saves an array of students to the Students folder.
	 * @param list  The students
	 */
	public void saveStudentArray(Student[] list)
		{
		for(int i = 0; i < list.length; i++)
			{
			saveStudent(new File("./Students/" + i + ".ser"), list[i]);
			}
		}
		
	/**
	 * Reads an array of students from the Students folder
	 * @return  The students.
	 */
	public Student[] readStudentArray()
		{
		File path = new File("./Students/");
		File[] files = path.listFiles();
		Student[] students = new Student[files.length];
		for(int i = 0; i < files.length; i++)
			{
			students[i] = readStudent(files[i]);	
			}
		return students;
		}
	
	/**
	 * Saves a single student's object to the given location.
	 * @param file  The path to save at.
	 * @param s  The student object.
	 */
	private void saveStudent(File file, Student s)
		{
		try
			{
			FileOutputStream fileOut =
			new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(s);
			out.close();
			fileOut.close();
			}
		catch(IOException i)
			{
			i.printStackTrace();
			}
		}
	
	/**
	 * Reads a student's object from a file.
	 * @param file  The location of the object.
	 * @return The student object
	 */
	private Student readStudent(File file)
		{
		try
			{
	        FileInputStream fileIn = new FileInputStream(file);
	        ObjectInputStream in = new ObjectInputStream(fileIn);
	        Student s = (Student) in.readObject();
	        in.close();
	        fileIn.close();
	        return s;
	        }
		catch(IOException i)
			{
	        i.printStackTrace();
	        return null;
	        }
		catch(ClassNotFoundException c)
			{
	        System.out.println("Student class not found");
	        c.printStackTrace();
	        return null;
			}
		}
	}
