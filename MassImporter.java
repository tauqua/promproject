
import java.util.*;
import java.io.*;
/**
*Manages importing students and their data from a text file
*@author Pranav Sai
*@version 2/7/2016
*/
public class MassImporter
	{
	//The list of students that is being generated from the text file
	private ArrayList<Student> students;
	
	/**
	*Constructor
	*/
	public MassImporter()
		{
		students = new ArrayList<Student>();	
		}
		
	/**
	*Basically chops up the text file and turns the data in there into a Student object
	*@return the list of the newly generated students
	*/
	public ArrayList<Student> collectData() throws IOException
		{
		//Reading the text file
		Scanner inputFile = new Scanner(new File("MassImport.txt"));
		
		//This loop cycles through all the lines of the text file
		while(inputFile.hasNextLine())
			{
			//One line of the text file
			String data  = inputFile.nextLine();
			
			boolean done = false;
			
			//i is position of the pointer on the line
			int i = 0;
			
			String name = "";
			
			int grade;
			
			//Gets the name of the person, stopping once it hits a number signifying that it is the end of the name
			while(!done)
				{
				if(data.charAt(i) == '1' || data.charAt(i) == '9')
					{
					done = true;	
					}
				else
					{
					name = name + data.charAt(i);
					i++;
					}	
				}
				
			//Takes care of parsing out the grade
			if(data.charAt(i) == '9')
				{
				grade = 9;
				i = i + 3;	
				}
			else
				{
				grade = Integer.parseInt(data.substring(i, i + 2));
				i = i + 4;	
				}
			String[] answers = new String[24];
			for(int j = 0; j < 24; j++)
				{
				answers[j] = Character.toString(data.charAt(i + j));	
				}
			for(int j = 0; j < answers.length; j++)
				{
				answers[j] = answers[j].toLowerCase();
				}
			students.add(new Student(name, answers, grade));
			}
		inputFile.close();
//		System.out.println(name + " " + );
		return students;
		}
	}