import java.io.Serializable;

public class Student implements Serializable
	{
	private Student[] matches = new Student[140];
	private int[] spectrumScores = new int[8];
	private String studentName;
	private int grade;
	private String[] answers = new String[24];
	
	public Student(String name, String[] responses, int gradeNum)
		{
		studentName = name;
		answers = responses;
		grade = gradeNum;	
		}
		
	public void analyzeAnswers()
		{
		for(int i = 0; i < answers.length; i += 3)
			{
			int score = 0;
			for(int j = i; j < i + 3; j++)
				{
				if(answers[j].equalsIgnoreCase("a"))
					{
					score += 3;	
					}
				else if(answers[j].equalsIgnoreCase("b"))
					{
					score += 1;	
					}
				else if(answers[j].equalsIgnoreCase("d"))
					{
					score -= 1;	
					}
				else if(answers[j].equalsIgnoreCase("e"))
					{
					score -= 3;	
					}
				}
			spectrumScores[i / 3] = score; 	
			}
		}
	
	public int[] getSpectrums()
		{
		return spectrumScores;	
		}
		
	public String[] getAnswers()
		{
		return answers;	
		}
	
	public String getName()
		{
		return studentName;	
		}
		
	public int getGrade()
		{
		return grade;	
		}
	
	public void setSpectrums(int[] spectrums)
		{
		spectrumScores = spectrums;	
		}
		
	public void setAnswers(String[] responses)
		{
		answers = responses;	
		}
		
	public void setName(String newName)
		{
		studentName = newName;	
		}
		
	public void setGrade(int newGrade)
		{
		grade = newGrade;	
		}	
	}