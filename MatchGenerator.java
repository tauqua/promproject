public class MatchGenerator
	{
	
	/**
	 * Generates the matches, given a difference matrix
	 * @author William Wang and Ben Simon
	 * @version 2/7/17
	 */
	public MatchGenerator()
		{
		//Database is handled by MenuDriver. There's no need for it here. Removed it.
		//MenuDriver passes Database.getDiffsMatrix, so that's what should be used.
		}
	
	public int[][] generateMatches(int[][] diffsMatrix)
		{
		int[][] results = new int[diffsMatrix.length][5];
		for(int i = 0; i < diffsMatrix.length; i++)
			{
			results[i] = generateMatch(diffsMatrix[i], i);
			}
		return results;
		}
		
	public int[] generateMatch(int[] diffs, int stuindex)
		{
		int[] minIndexes= {-1, -1, -1, -1, -1};
		for(int i = 0; i < diffs.length; i++)
			{
			if(diffs[minIndexes[0]] > diffs[i] && i != stuindex)
				minIndexes[0] = i;
			}
		for(int i = 0; i < diffs.length; i++)
			{
			if(diffs[minIndexes[1]] > diffs[i] && i != minIndexes[0] && i != stuindex)
				minIndexes[1] = i;
			}
		for(int i = 0; i < diffs.length; i++)
			{
			if(diffs[minIndexes[2]] > diffs[i] && i != minIndexes[1] && i != minIndexes[0] && i != stuindex)
				minIndexes[2] = i;
			}
		for(int i = 0; i < diffs.length; i++)
			{
			if(diffs[minIndexes[3]] > diffs[i] && i != minIndexes[2]  && i != minIndexes[1] && i != minIndexes[0] && i != stuindex)
				minIndexes[3] = i;
			}	
		for(int i = 0; i < diffs.length; i++)
			{
			if(diffs[minIndexes[4]] > diffs[i] && i != minIndexes[3]  && i != minIndexes[2] && i != minIndexes[1] && i != minIndexes[0] && i != stuindex)
				minIndexes[4] = i;
			}
		return minIndexes;
		}
		
	public int[][] newGenerateMatches(int[][] diffs)	
		{
		int[][] finalMatches = new int[diffs.length][5]; 
		for(int i = 0; i < diffs.length; i++)
			{
			int[] matches = {-1, -1, -1, -1, -1};
			int p = 0;
			for(int j = 0; j < diffs.length; j++)
				{
				if(j == i)
					{
					//Changing loop value inside the loop. Good job
					j++;	
					}
				if(p < 5)
					{
					int o = 0;
					while(matches[o] != -1)
						{
						o++;	
						}
					matches[o] = j;	
					}
				else
					{
					int max = matches[0];
					int maxPos = 0;
					for(int y = 0; y < matches.length; y++)
						{
						if(matches[y] > max)
							{
							max = matches[y];
							maxPos = y;	
							}	
						}
					//This gives an ArrayIndexOutOfBoundsException
					if(diffs[i][j] <= max)
						{
						matches[maxPos] = j;	
						}	
					}
				p++;	
				}	
			}	
		return finalMatches;
		}
	
	
	/**
	 * The actually functional method:
	 * Generates 5 matches in each area (overall best, overall worst, class best, class worst)
	 * @param diffs  The difference matrix, with an additional value for that student's grade
	 * @return The matches
	 */
	public int[][] newNewGenerateMatches(int[][]diffs)
		{
		int[][] results = new int[diffs.length][10];
		for(int i = 0; i < diffs.length; i++)
			{
			int[] personResult = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
			
			//Gets best overall matches
			for(int j = 0; j < 5; j++)
				{
				int lowest = -1;
				int lowestIndex = -1;
						
				for(int k = 0; k < diffs.length; k++)
					{
					if(k != i && (lowest >= diffs[i][k] || lowest == -1))
						{
						boolean notUsed = true;
						for(int l = 0; l < j; l++)
							{
							if(personResult[l] == k)
								notUsed = false;
							}
						if(notUsed)
							{
							lowest = diffs[i][k];
							lowestIndex = k;
							}
						}
					}
				personResult[j] = lowestIndex;
				}
			
			//Gets best matches within grade
			for(int j = 5; j < 10; j++)
				{
				int lowest = -1;
				int lowestIndex = -1;
				int grade = diffs[i][diffs[0].length - 1];
				
				for(int k = 0; k < diffs.length; k++)
					{
					if(k != i && (lowest >= diffs[i][k] || lowest == -1)  && (diffs[k][diffs[0].length - 1] == grade))
						{
						boolean notUsed = true;
						for(int l = 5; l < j; l++)
							{
							if(personResult[l] == k)
								notUsed = false;
							}
						if(notUsed)
							{
							lowest = diffs[i][k];
							lowestIndex = k;
							}
						}
					}
				personResult[j] = lowestIndex;
				}
			results[i] = personResult;
			
			//Gets worst overall matches
			for(int j = 10; j < 15; j++)
				{
				int greatest = -1;
				int greatestIndex = -1;
						
				for(int k = 0; k < diffs.length; k++)
					{
					if(k != i && (greatest <= diffs[i][k] || greatest == -1))
						{
						boolean notUsed = true;
						for(int l = 10; l < j; l++)
							{
							if(personResult[l] == k)
								notUsed = false;
							}
						if(notUsed)
							{
							greatest = diffs[i][k];
							greatestIndex = k;
							}
						}
					}
				personResult[j] = greatestIndex;
				}
			
			//Gets worst matches within grade
			for(int j = 15; j < 20; j++)
				{
				int greatest = -1;
				int greatestIndex = -1;
				int grade = diffs[i][diffs[0].length - 1];
				
				for(int k = 0; k < diffs.length; k++)
					{
					if(k != i && (greatest <= diffs[i][k] || greatest == -1)  && (diffs[k][diffs[0].length - 1] == grade))
						{
						boolean notUsed = true;
						for(int l = 15; l < j; l++)
							{
							if(personResult[l] == k)
								notUsed = false;
							}
						if(notUsed)
							{
							greatest = diffs[i][k];
							greatestIndex = k;
							}
						}
					}
				personResult[j] = greatestIndex;
				}
			results[i] = personResult;
			}
		
		return results;
		}
	}