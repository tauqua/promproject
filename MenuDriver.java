import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JOptionPane;

/**
 * Driver for the main class
 * @author Ben Simon and Prudhvie Gudapati
 * @version 2/8/17
 * NOTE: If saving students gives an error, try deleting and recreating the Students folder.
 */
public class MenuDriver
	{

	public static void main(String[] args)
		{
		
		//This old menu code courtesy of 2013 Ben
		boolean isOn = true;
		ArrayList<Student> students = new ArrayList<Student>();
		Saver fileHandler = new Saver();
		String[] options = new String[]{"Exit", "Read Students", "Save Students", "List Students", "Process Students",  "Remove Students", "Add Students", "Mass Import Students"};
		while(isOn)
			{
			int n = JOptionPane.showOptionDialog(null, "Choose an option", "Matchmaker", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			switch(n)
				{
				case 0: isOn = false;
						break;
				case 1: int result = JOptionPane.showConfirmDialog(null, "This will erase the current array. Do you want to continue?");
						if(result == 0)
							students = new ArrayList<Student>(Arrays.asList(fileHandler.readStudentArray()));
						break;
				case 2: Student[] studentsArray = new Student[students.size()];
						for(int i = 0; i < studentsArray.length; i++)
							{
							studentsArray[i] = students.get(i);
							}
						fileHandler.saveStudentArray(studentsArray);
						break;
				case 3: for(int i = 0; i < students.size(); i++)
							{
							Student current = students.get(i);
							//TODO Convert to window
							System.out.println(i + ":");
							System.out.println("Name: " + current.getName());
							//TODO Print the actual answers instead of the reference
							System.out.println("Answers: " + current.getAnswers());
							System.out.println("Grade: " + current.getGrade());
							}
					break;
				case 4: Student[] studentsBIArray = new Student[students.size()];
						for(int i = 0; i < studentsBIArray.length; i++)
							{
							studentsBIArray[i] = students.get(i);
							}
						makeMatches(studentsBIArray);
					break;
				case 5: int index = Integer.parseInt(JOptionPane.showInputDialog("Remove which student (Index)"));
						students.remove(index);
					break;
				case 6: String name = JOptionPane.showInputDialog("Enter the student's name");
						String[] answers = takeAnswers();
						int grade = Integer.parseInt(JOptionPane.showInputDialog("What grade is the student in (Number)"));
						students.add(new Student(name, answers, grade));
					break;
				case 7: MassImporter importer = new MassImporter();
						try
							{
							students = importer.collectData();
							}
						catch (IOException e)
							{
							e.printStackTrace();
							}
				}
			}
		}
	
	private static String[] takeAnswers()
		{
		String answers = JOptionPane.showInputDialog("Enter the answers, with no spaces in between them");
		//TODO: Add checking to make sure proper number of answers are sent
		String[] answerArray = new String[answers.length()];
		for(int i = 0; i < answers.length(); i++)
			{
			answerArray[i] = answers.substring(i, i+1);
			}
		return answerArray;
		}
	
	
	private static void makeMatches(Student[] students)
		{
		for(int i = 0; i < students.length; i++)
			students[i].analyzeAnswers();
		Database database = new Database(students);
		database.initializeDiffsMatrix();
		MatchGenerator matchmaker = new MatchGenerator();
		int[][] results = matchmaker.newNewGenerateMatches(database.getDiffsMatrix());
		
		try
			{
			writeToFile(results, students);
			}
		catch (IOException e)
			{
			e.printStackTrace();
			}
		/*
		for(int i = 0; i < students.length; i++)
			{
			System.out.println(students[i].getName());
			System.out.println("Overall Matches:");
			for(int j = 0; j < 5; j++)
				{
				System.out.println(students[results[i][j]].getName());
				}
			System.out.println("Grade matches");
			for(int j = 5; j < 10; j++)
				{
				System.out.println(students[results[i][j]].getName());
				}
			System.out.println();
			}
		 */
		}
	
	public static void writeToFile(int[][] results, Student[] students) throws IOException
		{
		PrintWriter writer = new PrintWriter("./results.txt");
		for(int i = 0; i < students.length; i++)
			{
			writer.println("--------------------");
			writer.println(students[i].getName());
			writer.println("Best Overall Matches:");
			for(int j = 0; j < 5; j++)
				{
				writer.println(students[results[i][j]].getName());
				}
			writer.println();
			writer.println("Best Grade Matches");
			for(int j = 5; j < 10; j++)
				{
				writer.println(students[results[i][j]].getName());
				}
			writer.println();
			writer.println("Worst Overall Matches");
			for(int j = 10; j < 15; j++)
				{
				writer.println(students[results[i][j]].getName());
				}
			writer.println();
			writer.println("Worst Grade Matches");
			for(int j = 15; j < 20; j++)
				{
				writer.println(students[results[i][j]].getName());
				}
			writer.println();
			writer.println();
			}
		writer.close();
		}
	}
