
/**
 * This class maintains all the students info and process them to get a difference matrix that will be later used for match maker.
 * @version 12-12-2016
 */

public class Database
	{
	private int stunum; //Unecessary, but I have this variable here anyway.
	private Student[] students; //The array contains all the student objects.
	private int[][] diffs; //The differences matrix
	private int currentnum = 0; //Used to count the number of students that have been added.
	
	/**
	 * Constructor.
	 * @param numofstudents Number of students.
	 */
	public Database(int numofstudents)
		{
		stunum = numofstudents;
		students = new Student[stunum];
		diffs = new int[stunum][stunum + 1];
		}
	
	/**
	 * Constructor.
	 * @param stus An student array contains all the students.
	 */
	public Database(Student[] stus)
		{
		stunum = stus.length;
		students = stus;
		diffs = new int[stunum][stunum + 1];
		}
	
	/**
	 * This method adds a student to the database.
	 * @param stu The student's object.
	 */
	public void add(Student stu)
		{
		if(currentnum < stunum)
			students[currentnum] = stu;
		else
			{
			Student[] newstudents = new Student[students.length + 1];
			for(int i = 0; i < students.length; i++)
				newstudents[i] = students[i];
			newstudents[students.length] = stu;
			stunum++;
			}
		currentnum++;
		}
	
	/**
	 * This method removes a student from the database.
	 * @param name The student's name.
	 */
	public void remove(String name)
		{
		int[] newlist = new int[stunum];
		int i = 0;
		while(!students[i].equals(name))
			i++;
		for(i = i; i < stunum - 1; i++)
			students[i] = students[i + 1];
		students[stunum - 1] = null;
		}
	
	/**
	 * This method calculates the how similar each student is to every other in terms of scores
	 * And stores the scores in the matrix diffs 
	 * The last item in the array for each student will be the student's grade number.
	 * With high scores representing dissimilarity and low scores meaning the two students are potential matches.
	 */
	public void initializeDiffsMatrix()
		{
		//i and j are indexes for two students in the students array.
		for(int i = 0; i < stunum; i++)
			{
			diffs[i][i] = 0;
			for(int j = i + 1; j < stunum; j++)
				{
				int[] spectrum1 = students[i].getSpectrums();
				int[] spectrum2 = students[j].getSpectrums();
				int diff = 0;
				for(int k = 0; k < 8; k++)
					diff += (spectrum1[k] - spectrum2[k]) * (spectrum1[k] - spectrum2[k]);
				diffs[i][j] = diff;
				diffs[j][i] = diff;
				}
			diffs[i][stunum] = students[i].getGrade();
			}
		}
	
	/**
	 * This method returns the differences matrix.
	 * @return The differences matrix.
	 */
	public int[][] getDiffsMatrix()
		{
		return diffs;
		}
		
	public int getNumStu()
		{
		return stunum;	
		}
	
	/**
	 * This method prints the list of students.
	 */
	public void printStudents()
		{
		int i = 0;
		while(students[i] != null)
			{
			System.out.println("Student " + (i + 1) + ": " + students[i].getName());
			int[] spectrumscores = students[i].getSpectrums();
			for(int j = 0; j < 8; j++)
				System.out.println("Spectrum " + (j + 1) + ": " + spectrumscores[j]);
			System.out.println();
			i++;
			}
		}
	}