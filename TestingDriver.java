
/**
 * @author Ben Simon
 * @version 12/13/16
 * Used for testing smaller aspects of the program
 */
public class TestingDriver
	{

	public static void main(String[] args)
		{
		//Testing Saver
		Saver fileHandler = new Saver();
		
		Student[] list = new Student[]{new Student("Bob", new String[]{"B"}, 2), new Student("Bob2", new String[]{"A"}, 3)};
		fileHandler.saveStudentArray(list);
		
		Student[] list2 = fileHandler.readStudentArray();
		
		for(int i = 0; i < list2.length; i++)
			{
			System.out.println(list2[i].getName());
			System.out.println(list2[i].getGrade());
			System.out.println(list2[i].getAnswers());
			System.out.println();
			}
		}

	}
